
 php artisan make:controller UserController => Para crear el controlador UserController

 php artisan tinker => consola interna para mannipular datos
 factory(App\Users::class, 12)->create()     => Para crear 12 registros de prueba en la tabla User

 
 Route::resource('pages', 'PageController');  => En rutas crea 7 rutas posibles (Es un standart)
 php artisan make:controller PageController --resource  => Crea los 7 metodos posibles en el controlador
 php artisan make:controller PageController --resource --model=Page  =>	Crea el Controlador con los 7 metodos y el modelo llamado Page

 php artisan make:request PostRequest    => Crea una carpeta request y dentro un archivo PostRequest donde deben ir las validaciones
						de un formulario y no en el controlador.(Aislar la logica)

 composer require laravel/ui:^1.0 --dev  => para tener el paquete de UI para tener disponible el sistema de login y de registro de UI
 php artisan ui bootstrap --auth   => para instalar el sistema de login y autenticación con bootstrap
 php artisan vue --auth   => para instalar el sistema de login y autenticación con Vue

 php artisan make:model  -h  => Proporciona ayuda para ver las opciones que quieres crear con el modelo
 Ejemplo:  php artisan make:model Post -m -f => Crea el modelo Post con el Factory y las migraciones.
 (Es una convención crear el modelo en ingles y en singular eso ayuda a crear la tabla en plural

 php artisan migrate:refresh  => Refresca las migraciones (Elimina los datos)
 
  php artisan migrate:refresh --seed    => Para crear las tablas y se carguen los datos semilla

 composer require cviebrock/eloquent-sluggable   => Instalacion de un componente para crear slugs (Colocar url en forma amigable)


<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Post;
use App\User;

Route::get('eloquent', function () {
   // $posts = Post::all();
    $posts = Post::where('id','>=','20')
    ->orderBy('id','desc')
    ->take(3)
    ->get();

    foreach ($posts as $post) {
        echo "$post->id $post->title <br>";
    }
});

Route::get('posts', function () {
    $posts = Post::get();

    foreach ($posts as $post) {
        echo "
            $post->id
            <strong>{$post->user->get_name}</strong>,
            $post->get_title <br>
            ";
    }


    // // $posts = Post::all();
    // $posts = Post::with(['user'=>function($u){
    //     $u->orderBy('name');
    // }])->orderBy('users.name')->get()
    // ;
    // //orderBy('autor')->get();
    //  //$posts = Post::all();


    //  foreach ($posts as $post) {
    //      echo "
    //     $post->id
    //     $post->title
    //     {$post->user->name} <br>";
    //  }
 });


 Route::get('users', function () {
    // $posts = Post::all();
     $users = User::get();

     foreach ($users as $user) {
         echo "
        $user->id
        $user->get_name
        <strong>{$user->posts->count()}</strong>
        <br>";
     }
 });

 Route::get('collections', function () {

     $users = User::all();

    //dd($users);
    //dd($users->contains(4));
    //dd($users->except([1,2,3]));
    //dd($users->only(4));
    //dd($users->find(4));
    dd($users->load('posts'));
 });

 Route::get('serialization', function () {

    $users = User::all();

   //dd($users->toArray());
   $user = $users->find(1);
   //dd($user);
   dd($user->toJson());
});
